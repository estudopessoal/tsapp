class SpellCard extends Card {

  constructor(id: number, name: string, image: string, description: string) {
    super(id, 'Spell', name, image, description)
  }

}