const deck = new Deck([
  //new MonsterCard(1, 'Dark Magician', '1.jpg', '...', 'Dark', 7, 2500, 2100),
  //new MonsterCard(2, 'Dark Blau', '2.jpg', '...', 'Dark', 4, 3500, 100),
  new SpellCard(3, 'Lalala', '3.jpg', '...'),
  new SpellCard(4, 'JSJSjs', '4.jpg', '...'),
  new TrapCard(5, 'TETEte', '5.jpg', '...')
])

const allan = new Player(8000, deck)

allan.hand = [
  new MonsterCard(1, 'Dark Magician', '1.jpg', '...', 'Dark', 7, 2500, 2100),
  new MonsterCard(2, 'Dark Blau', '2.jpg', '...', 'Dark', 4, 3500, 100)
]


console.log('deck:')
console.log(allan.deck.cards)
console.log('mão:')
console.log(allan.hand)
console.log('=====')

allan.getCardFromDeck()

console.log('deck:')
console.log(allan.deck.cards)
console.log('mão:')
console.log(allan.hand)
console.log('=====')