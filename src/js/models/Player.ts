class Player {

  private _lifePoints:number
  private _deck:Deck
  private _hand:Array<Card> = []

  constructor(lifePoints:number, deck:Deck) {
    this._lifePoints = lifePoints
    this._deck = deck
  }

  public get lifePoints() :number {
    return this._lifePoints
  }

  public get deck() :Deck {
    return this._deck
  }

  public set hand(cards:Array<Card>) {
    this._hand = cards
  }

  public get hand() :Array<Card> {
    return this._hand
  }

  removeCardFromDeck(cardIndex:number) {
    this._deck.cards.splice(cardIndex)
  }

  removeCardsFromDeck(cardsIndex:Array<number>) {
    cardsIndex.forEach(card => this._deck.cards.splice(card))
  }

  getCardFromDeck() {
    const fullHand: number = 5
    if (this._hand.length >= fullHand)
      return

    this._hand.push(this._deck.cards[0])
    this.removeCardFromDeck(0)  
  }

  getfullHandFromDeck() {

    const fullHand:number = 5
    if(this._hand.length >= fullHand)
      return
    
    let removeIndexCards:Array<number> = []

    this._deck.cards.map((card,index) => {
      
      if(this._hand.length <= fullHand){
        this._hand.push(card)
        removeIndexCards.push(index)
      }

    })

    this.removeCardsFromDeck(removeIndexCards)    

  }

}