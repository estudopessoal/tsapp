class Card {

  private _id:number
  private _type:string
  private _name:string
  private _image:string
  private _description:string

  constructor(id:number, type:string, name:string, image:string, description:string) {
    this._id = id
    this._type = type
    this._name = name
    this._image = image
    this._description = description
  }

  public get id() {
    return this._id
  }

  public get type() {
    return this._type
  }

  public get name() {
    return this._name
  }

  public get image() {
    return this._image
  }

  public get description() {
    return this._description
  }

}