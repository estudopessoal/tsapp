class Deck {

  private _cards:Array<Card>

  constructor(cards:Array<Card>) {
    this._cards = this._sortCards(cards)
  }

  get cards() {
    return this._cards
  }

  private _sortCards(cards:Array<Card>):Array<Card> {
    return _.shuffle(cards)
  }
}