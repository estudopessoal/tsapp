class TrapCard extends Card {
  
  constructor(id: number, name: string, image: string, description: string) {
    super(id, 'Trap', name, image, description)
  }
  
}