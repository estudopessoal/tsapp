class MonsterCard extends Card {

  private _property:string
  private _level:number
  private _attackPoints:number
  private _defensePoints:number

  constructor(id:number, name:string, image:string, description:string, property:string, level:number, attackPoints:number, defensePoints:number) {
    super(id, 'Monster', name, image, description)

    this._property = property
    this._level = level
    this._attackPoints = attackPoints
    this._defensePoints = defensePoints
  }

  public get property() {
    return this._property
  }

  public get level() {
    return this._level
  }

  public get attackPoints() {
    return this._attackPoints
  }

  public get defensePoints() {
    return this._defensePoints
  }
  
}